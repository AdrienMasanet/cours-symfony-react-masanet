# Ne surtout pas oublier qu'il faut installer le apache-pack pour que le serveur ne soit pas limité au local
lando composer req apache-pack

# Créer un nouveau projet symfony mais pas en mode application web, ce qui installerait beaucoup trop de librairies inutiles.
composer create-project symfony/skeleton my_project_name

# Installer la librairie de gestion du CORS :
lando composer req cors
elle se configure dans .env (il y a un exemple en commentaire dans ce fichier)

# Installer la librairie de gestion d'api qui permet de réaliser une api efficace et aux normes
lando composer req api
elle se configure dans les fichiers Entity avec les annotations (ne pas oublier les use en haut du fichier Entity en fonction des annotations)
On peut exécuter la commande lando console debug:router pour voir la liste de toutes les routes de notre application Symfony, pratique étant donné le nombre de routes que peut générer cette librairie

# lando composer req symfony/serializer
Cette librairie aide l'api a serializer et déserialiser l'objet en JSON