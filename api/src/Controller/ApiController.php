<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Personne;

class ApiController extends AbstractController
{
  private $personRepo;

  function __construct()
  {
    $this->personRepo = $this->getDoctrine()->getRepository(Personne::class);
  }

  // C'est comme ça qu'il faut créer une route qui retourne du JSON dans le cas où l'on
  // ne voudrait pas utiliser le bundle api pour Symfony
  // Là je l'ai laissé mais c'est juste pour l'exemple, le bundle fait tout tout seul en vrai
  /**
   * @Route("/listperson", name="listperson")
   * @return JsonResponse
   */
  public function getPerson(): Response
  {
    $persons = $this->personRepo->findAll();
    return $this->json([
      'persons' => $persons
    ]);
  }
}
