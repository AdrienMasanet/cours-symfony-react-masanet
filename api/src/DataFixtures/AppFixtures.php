<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Personne;

class AppFixtures extends Fixture
{
  private $faker;

  public function __construct()
  {
    $this->faker = \Faker\Factory::create();
  }

  public function load(ObjectManager $manager)
  {
    // Génération des fixtures de type "Personne"
    for ($i = 0; $i < 10; $i++) {
      $person = new Personne();
      $person->setNom($this->faker->lastName)
        ->setPrenom($this->faker->firstName)
        ->setAge($this->faker->numberBetween(5, 99));
      // On rajoute la personne à la file d'attente des données en attente d'être écrites dans la bdd
      $manager->persist($person);
    }


    // On flush toutes les fixtures précédemment "persistées" afin de les écrire dans la bdd
    $manager->flush();
  }
}
