// Ce fichier permet la configuration d'axios, ça permet d'éviter de refaire la configuration dans chaque composant
import axios from "axios";

export default axios.create({
  baseURL: "https://messagerie_api.lndo.site:444/api/"
});