import React, { Component } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import Connecteur from "../connecteur/Connecteur"
import Formulaire from "./Formulaire";
import Message from "./Message";

export default class App extends Component
{

  state = {
    pseudo: this.props.match.params.pseudo,
    messages: {}
  }

  messageRef = React.createRef();

  addMessage = message =>
  {
    Connecteur.post( "/messages", message ).then(
      res =>
      {
        const messages = { ...this.state.messages }
        messages[ res.data.id ] = res.data;
        this.setState( { messages } );
      }
    );
  }

  isUser = pseudo => pseudo === this.state.pseudo;

  componentDidMount = async () =>
  {
    await Connecteur.get( "/messages", {} ).then(
      res =>
      {
        this.setState( { messages: res.data[ "hydra:member" ] } );
      }
    );
  }

  componentDidUpdate ()
  {
    const ref = this.messageRef.current
    ref.scrollTop = ref.scrollHeight
  }

  render ()
  {
    const messages = Object.keys( this.state.messages ).map( key =>
    (
      <CSSTransition key={ this.state.messages[ key ].id } timeout={ 200 } className="fade">
        <Message isUser={ this.isUser } pseudo={ this.state.messages[ key ].pseudo } message={ this.state.messages[ key ].message } />
      </CSSTransition>
    ) );

    return (
      <div className="box">
        <div className="messages" ref={ this.messageRef }>
          <TransitionGroup className="message" >
            { messages }
          </TransitionGroup>
        </div>
        <Formulaire length={ 140 } pseudo={ this.state.pseudo } addMessage={ this.addMessage } />
      </div >
    );
  }
}

