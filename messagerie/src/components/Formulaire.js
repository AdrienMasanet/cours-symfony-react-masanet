import React, { Component } from "react";

export default class Formulaire extends Component
{
  state = {
    message: "",
    length: this.props.length
  }

  createMessage = () =>
  {
    // On assigne ces nouvelles variables aux variables correspondantes sur props
    const { addMessage, pseudo, length } = this.props;
    const message = {
      pseudo: pseudo,
      message: this.state.message
    }
    addMessage( message );
    this.setState( { message: "", length } );
  }

  handleChange = event =>
  {
    const length = this.props.length - event.target.value.length;
    this.setState( { message: event.target.value, length: length } );
  }

  handleKey = event =>
  {
    if ( event.key === "Enter" ) {
      this.createMessage();
    }
  }

  handleSubmit = event =>
  {
    event.preventDefault();
    this.createMessage();
  }

  render ()
  {
    return (
      <form className="form" onSubmit={ this.handleSubmit }>
        <textarea required maxLength="140" value={ this.state.message } onChange={ this.handleChange } onKeyUp={ this.handleKey } />
        <div className="info">{ this.state.length }</div>
        <button type="submit">Envoyer !</button>
      </form>
    );
  }
}