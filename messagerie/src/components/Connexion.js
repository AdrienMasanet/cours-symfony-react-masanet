import React, { Component } from "react";
import { Redirect } from "react-router-dom";

export default class Connexion extends Component
{
  state = {
    pseudo: "",
    password: "",
    goToChat: false
  }

  handleChange = event =>
  {
    // On assigne la valeur qui a changé à la propriété correspondante dans le state
    // Les crochets pour event.target.name servent a nommer dynamiquement une référence
    this.setState( { [ event.target.name ]: event.target.value } );

    console.log( this.state );
  }

  handleSumbit = event =>
  {
    event.preventDefault();
    this.setState( { goToChat: true } );
  }

  render ()
  {
    if ( this.state.goToChat ) {
      return <Redirect push to={ `/pseudo/${ this.state.pseudo }` } />
    } else {
      return (
        <div className="connexionBox">
          <form onSubmit={ this.handleSumbit } className="connexion">
            <input type="text" name="pseudo" value={ this.state.pseudo } onChange={ this.handleChange } placeholder="Identifiant" required />
            <input type="password" name="password" value={ this.state.password } onChange={ this.handleChange } placeholder="Mot de passe" required />
            <button type="submit">GO</button>
          </form>
        </div>
      );
    }
  }
}