import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import NotFound from "./components/NotFound";
import Connexion from "./components/Connexion";
import App from "./components/App";

import "./app.css";
import "./index.css";
import "./animation.css";

const Routing = () =>
{
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={ Connexion } />
        <Route path="/pseudo/:pseudo" component={ App } />
        <Route component={ NotFound } />
      </Switch>
    </BrowserRouter>
  );
}

ReactDOM.render( <Routing />, document.querySelector( "#root" ) );